//final commit

#include<stdio.h>
void swap(int *,int *);

int main()
{
  int a,b;
  printf("Enter the two numbers a and b\n");
  scanf("%d %d",&a,&b);
  swap(&a,&b);
  printf("Values of a and b after swapping are %d and %d",a,b);
  return 0;
}

void swap(int *a,int *b)
   {
     int temp;
     temp=*a;
     *a=*b;
     *b=temp;
   }

